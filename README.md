# 4. CKA Pratice Exam Part 4 ACG : Sauvegarde et Restauration des Données ETCD


------------

><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

# Objectifs

1. Sauvegarder les données etcd

2. Restaurer les données etcd à partir de la sauvegarde

# Contexte

Cet atelier propose des scénarios pratiques pour vous aider à vous préparer à l'examen Certified Kubernetes Administrator (CKA). Des tâches à accomplir vous seront présentées ainsi que des serveurs et/ou un cluster Kubernetes existant pour les accomplir. Vous devrez utiliser vos connaissances de Kubernetes pour mener à bien les tâches fournies, un peu comme vous le feriez sur le terrain réel. Examen CKA. Bonne chance!

Chacun des objectifs représente une tâche que vous devrez accomplir en utilisant le cluster et les serveurs disponibles. Lisez attentivement chaque objectif et accomplissez la tâche spécifiée.

Pour cette question, vous devrez vous connecter au ectd1serveur. Vous pouvez le faire en utilisant le nom d'hôte (c'est-à-dire ssh etcd1). Lors du référencement etcd1, utilisez le nom d'hôte etcd1et nonlocalhost.

Remarque : Vous ne pouvez pas vous connecter à un autre nœud à partir d'un nœud autre que le nœud racine. Une fois que vous avez terminé les tâches nécessaires sur un serveur, assurez-vous de quitter et de revenir au nœud racine avant de continuer.

Si vous devez assumer les privilèges root sur un serveur, vous pouvez le faire avec sudo -i.

Vous pouvez exécuter le script de vérification situé sur /home/cloud_user/verify.shle etcd1serveur à tout moment pour vérifier votre travail !

>![Alt text](img/image.png)

# Introduction
Ce laboratoire décrit comment sauvegarder et restaurer les données ETCD, un composant essentiel de Kubernetes. Vous apprendrez à créer une sauvegarde des données ETCD, à supprimer les données existantes et à restaurer à partir de la sauvegarde. Suivez attentivement chaque étape pour réussir ces tâches.

# Application

## Étape 1 : Connexion au Serveur

1. Connectez-vous au serveur à l'aide des informations d'identification fournies :

```sh
ssh cloud_user@<PUBLIC_IP_ADDRESS>
```

## Étape 2 : Sauvegarder les Données ETCD

1. Depuis le terminal, connectez-vous au serveur ETCD :

```sh
ssh etcd1
```

2. Sauvegardez les données ETCD :

```yaml
ETCDCTL_API=3 etcdctl snapshot save /home/cloud_user/etcd_backup.db \
--endpoints=https://etcd1:2379 \
--cacert=/home/cloud_user/etcd-certs/etcd-ca.pem \
--cert=/home/cloud_user/etcd-certs/etcd-server.crt \
--key=/home/cloud_user/etcd-certs/etcd-server.key
```

>![Alt text](img/image-1.png)

Ref doc : 

https://kubernetes.io/docs/tasks/administer-cluster/configure-upgrade-etcd/#volume-snapshot

https://kubernetes.io/docs/tasks/administer-cluster/configure-upgrade-etcd/#backing-up-an-etcd-cluster

https://kubernetes.io/docs/tasks/administer-cluster/configure-upgrade-etcd/#securing-communication

3. Vérification de la sauvegarde 

```sh
etcdctl --write-out=table snapshot status etcd_backup.db 
```

>![Alt text](img/image-3.png)

## Étape 3 : Restaurer les Données ETCD à partir de la Sauvegarde

1. Arrêtez ETCD :

```sh
sudo systemctl stop etcd
```

2. Supprimez les données ETCD existantes :

```sh
sudo rm -rf /var/lib/etcd
```

>![Alt text](img/image-2.png)

3. Restaurer les données ETCD à partir d'une sauvegarde :

```sh
sudo ETCDCTL_API=3 etcdctl snapshot restore /home/cloud_user/etcd_backup.db \
--initial-cluster etcd-restore=https://etcd1:2380 \
--initial-advertise-peer-urls https://etcd1:2380 \
--name etcd-restore \
--data-dir /var/lib/etcd
```

>![Alt text](img/image-4.png)


Ref doc : 

https://kubernetes.io/docs/tasks/administer-cluster/configure-upgrade-etcd/#restoring-an-etcd-cluster


4. Définir la propriété de la base de données :

```sh
sudo chown -R etcd:etcd /var/lib/etcd
```

5. Démarrer ETCD :

```sh
sudo systemctl start etcd
```

>![Alt text](img/image-5.png)

## Étape 4 : Vérifier que le Système Fonctionne

1. Vérifiez que le système fonctionne :

```sh
ETCDCTL_API=3 etcdctl get cluster.name \
--endpoints=https://etcd1:2379 \
--cacert=/home/cloud_user/etcd-certs/etcd-ca.pem \
--cert=/home/cloud_user/etcd-certs/etcd-server.crt \
--key=/home/cloud_user/etcd-certs/etcd-server.key
```

>![Alt text](img/image-6.png)

En suivant ces étapes, vous aurez sauvegardé et restauré avec succès les données ETCD, assurant ainsi la continuité des services Kubernetes en cas de défaillance.